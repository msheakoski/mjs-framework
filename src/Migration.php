<?php

namespace MJS\Framework;
use Exception;
use PDOException;
use ReflectionObject;

class Migration {
	protected $dbh;

	function __construct() {
		$this->dbh = DB::dbh();
	}

	function run($direction = 'up') {
		$this->createSchemaMigrationsTable();
		if ($this->isAlreadyMigrated()) {
			echo 'skipping: ', $this->version(), ' ', get_class($this), "\n";
			return;
		} else {
			echo 'running: ', $this->version(), ' ', get_class($this), "\n";
		}
		try {
			if ($direction === 'up') {
				$this->up();
				$this->flagAsMigrated();
			} else {
				$this->down();
				$this->unflagAsMigrated();
			}
		} catch (Exception $e) {
			die('Error: ' . $e->getMessage());
		}
	}

	protected function up() {
		throw new Exception('Not Implemented');
	}

	protected function down() {
		throw new Exception('Not Implemented');
	}

	protected function enableHstoreExtension() {
		try {
			$hstore_sql = 'create extension if not exists hstore;';
			$this->dbh->exec($hstore_sql);
		} catch (PDOException $e) {
			$message = implode("\n", [
				"The {$_ENV['DB_USERNAME']} role does not have superuser privileges, which are",
				"required to enable the hstore extension. Run the following command as a",
				"superuser (postgres) for the migration to successfully complete:",
				"",
				"  {$hstore_sql}"
			]);
			throw new Exception($message);
		}
	}

	protected function createSchemaMigrationsTable() {
		try {
			$this->dbh->exec('
				create table if not exists schema_migrations (
					version timestamp without time zone not null
				)
			');
		} catch (PDOException $e) {
			die('Error: ' . $e->getMessage());
		}
	}

	protected function flagAsMigrated() {
		try {
			$sth = $this->dbh->prepare('
				insert into schema_migrations (version) values (?)
			');
			$sth->execute([$this->version()]);
		} catch (PDOException $e) {
			die('Error: ' . $e->getMessage());
		}
	}

	protected function unflagAsMigrated() {
		try {
			$sth = $this->dbh->prepare('
				delete from schema_migrations where version = ?
			');
			$sth->execute([$this->version()]);
		} catch (PDOException $e) {
			die('Error: ' . $e->getMessage());
		}
	}

	protected function version() {
		$ro = new ReflectionObject($this);
		$fname_parts = explode('_', basename($ro->getFileName()));
		$file_timestamp = array_shift($fname_parts);
		return strftime('%Y-%m-%d %H:%M:%S', strtotime($file_timestamp));
	}

	protected function isAlreadyMigrated() {
		$sth = $this->dbh->prepare('select count(*) from schema_migrations where version = ? limit 1');
		$sth->execute([$this->version()]);
		return !! $sth->fetchColumn(0);
	}
}
