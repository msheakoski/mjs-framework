<?php

namespace MJS\Framework;

class Request {
	function __construct() {
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->path = preg_replace('/\?(?:.+)?/', '', $_SERVER['REQUEST_URI']);
		$this->params = array_merge($_GET, $_POST);
		$this->env = $_SERVER;
		$this->cookies = $_COOKIE;
	}
}
