<?php

namespace MJS\Framework;
use PDO;

class DB {
	protected static $dbh;

	static function dbh() {
		if (! self::$dbh) {
			self::$dbh = new PDO(
				"pgsql:host={$_ENV['DB_HOST']};dbname={$_ENV['DB_NAME']}",
				$_ENV['DB_USERNAME'],
				$_ENV['DB_PASSWORD'],
				[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
			);
		}
		return self::$dbh;
	}
}
