<?php

namespace MJS\Framework;

class Router {
	protected $method;
	protected $path;
	protected $routes = [];

	function __construct($options = []) {
		$this->method = isset($options['method']) ? $options['method'] : $_SERVER['REQUEST_METHOD'];
		$this->path = isset($options['path']) ? $options['path'] : preg_replace('/\?(?:.+)?/', '', $_SERVER['REQUEST_URI']);
	}

	function addRoute($name, $options = []) {
		$this->routes[$name] = [
			'method' => isset($options['method']) ? $options['method'] : null,
			'pattern' => $options['pattern'],
			'template' => $options['template'],
			'action' => $options['action']
		];
	}

	function dispatch() {
		foreach ($this->routes as $route) {
			if (preg_match($route['pattern'], $this->path) &&
				(empty($route['method']) ||
				in_array($this->method, (array) $route['method']))
			) {
				if ($route['action']() === false) continue;
				exit;
			} else {
				continue;
			}
		}
	}

	function path($name, $params = []) {
		$template = $this->routes[$name]['template'];
		preg_match_all('/:([0-9a-z_]+)/', $template, $matches);
		$template_params = $matches[1];
		foreach ($template_params as $param) {
			$replacement = $params[$param];
			unset($params[$param]);
			$template = preg_replace("/:{$param}/", $replacement, $template);
		}
		$template = preg_replace('#/{2,}#', '/', $template);
		$template = $template . '?' . http_build_query($params);
		return preg_replace('/\?$/', '', $template);
	}
}
