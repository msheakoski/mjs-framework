<?php

namespace MJS\Framework;

class Utils {
	static function isBlank($value) {
		return empty($value) && ! is_numeric($value);
	}

	static function isPresent($value) {
		return ! self::isBlank($value);
	}

	static function stripIndents($str) {
		preg_match_all('/^[ \t]*(?=\S)/m', $str, $matches);
		$shortest_indent = strlen(min($matches[0]));
		return preg_replace("/^[ \t]{{$shortest_indent}}/m", '', $str);
	}

	static function randomString($length = 16) {
		$str = '';
		while (strlen($str) < $length) $str .= base_convert(rand(), 10, 36);
		return substr($str, 0, $length);
	}
}
