<?php

namespace MJS\Framework;

class Controller {
	function __construct($request) {
		$this->request = $request;
		$this->params = $request->params;
	}

	protected function setStatusCode($code, $text = null) {
		header($this->request->env['SERVER_PROTOCOL'] . rtrim(" {$code} {$text}"));
	}
}
