<?php

namespace MJS\Framework;

class Model {
	function __construct($attrs = []) {
		foreach ($attrs as $k => $v) $this->$k = $v;
	}
}
